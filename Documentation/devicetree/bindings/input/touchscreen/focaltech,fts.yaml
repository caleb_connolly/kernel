# SPDX-License-Identifier: (GPL-2.0-only OR BSD-2-Clause)
%YAML 1.2
---
$id: http://devicetree.org/schemas/input/touchscreen/focaltech,fts.yaml#
$schema: http://devicetree.org/meta-schemas/core.yaml#

title: Focaltech FTS I2C Touchscreen Controller

maintainers:
  - Joel Selvaraj <joelselvaraj.oss@gmail.com>
  - Caleb Connolly <caleb@connolly.tech>

allOf:
  - $ref: touchscreen.yaml#

properties:
  compatible:
    enum:
      - focaltech,fts5452
      - focaltech,fts8719
  reg:
    const: 0x38

  interrupts:
    maxItems: 1

  reset-gpios:
    maxItems: 1

  avdd-supply:
    description: a phandle for the regulator supplying analog power (2.6V to 3.3V).

  vddio-supply:
    description: a phandle for the regulator supplying IO power (1.8V).

  focaltech,max-touch-number:
    $ref: /schemas/types.yaml#/definitions/uint32
    description: max number of fingers supported
    minimum: 2
    maximum: 10

  touchscreen-size-x: true
  touchscreen-size-y: true

additionalProperties: false

required:
  - compatible
  - reg
  - reset-gpios
  - focaltech,max-touch-number
  - touchscreen-size-x
  - touchscreen-size-y

examples:
  - |
    #include <dt-bindings/interrupt-controller/irq.h>
    #include <dt-bindings/gpio/gpio.h>
    &i2c5 {
      status="okay";

      touchscreen: focaltech@38 {
        compatible = "focaltech,fts8719";
        reg = <0x38>;
        interrupt-parent = <&tlmm>;
        interrupts = <31 IRQ_TYPE_EDGE_RISING>;

        avdd-supply = <&vreg_l28a_3p0>;
        vddio-supply = <&vreg_l14a_1p8>;

        pinctrl-names = "default", "sleep";
        pinctrl-0 = <&ts_int_default &ts_reset_default>;
        pinctrl-1 = <&ts_int_sleep &ts_reset_sleep>;

        reset-gpios = <&tlmm 32 GPIO_ACTIVE_LOW>;

        touchscreen-size-x = <1080>;
        touchscreen-size-y = <2246>;
        focaltech,max-touch-number = <10>;
      };
    };
